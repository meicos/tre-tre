<?php

require('functions.php');

if(@$_POST['submit']) {

	$id = "";
	$name = $_POST['name'];
	$message = str_replace(' ', '+', $_POST['message']);

	$json = file_get_contents('http://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=ja&dt=t&q=' . $message);
	$text = json_decode($json);
	$new_message = $text[0][0][0];

	$save_message = new connect_db($id, $name, $new_message);
	$save_message->save_message();
	$save_message->display_message();
	$old_message = $save_message->row;

} else if(@$_POST['btn_delete']) {

	$id = $_POST['delete'];
	$old_message = 'shunpei';
	$delete_message = new connect_db($id, '', '');
	$delete_message->delete_message();
	$delete_message->display_message();
	if($delete_message->row){
	$old_message = $delete_message->row; 
	} 
	
} else {

	$id = $name = $new_message = '';
	$save_message = new connect_db($id, $name, $new_message);
	$save_message->display_message();
	$old_message = $save_message->row;
	
}
?>


<!DOCTYPE html>
<head>
<title>Shunpei's Chatbox</title>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>

<h1 class="text-center">Shunpei's Chatbox</h1>
<div class="container" style="margin-top: 50px;">
<div class="col-sm-offset-2 col-sm-8">

<form method="POST" action="" name="form">
<div class="form-group">
<label for="chatbox">Messages</label>



<table class="table table-bordered table-hover">
<tr>
<th>Name</th>
<th>Message (JA)</th>
<th>Message (EN)</th>
<th>Time</th>
<th>Delete</th>
</tr>

<?php 
	if(is_array($old_message)) {
		foreach($old_message as $count=>$value) {
		echo '<tr>'; 
		echo '<td>' . $value['name'] . '</td>'; 
		echo '<td id="message_ja_' . $count . '">' . $value['message'] . '</td>'; 
		echo '<td id="message_en_' . $count . '"></td>';
		echo '<td>' . $value['timestamp'] . '</td>'; 
		echo '<td class="text-center">'.'<input type="checkbox" name="delete[]" onClick="enableDelete()" id="delete" value = "' . $value['id'] . '"' .'</td></tr>';
		}
	}
?>

</table>

<label for="name" style="margin-top: 50px">Name:</label>
<input type="text" name="name" id="name" class="form-control" required>

<label for="message">Your Message:</label>
<textarea name="message" id="message" class="form-control" placeholder="Your Message Here" rows="6"></textarea>

<div class="text-center" style="margin-top: 20px">
<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Submit">
<input type="reset" class="btn btn-primary" value="Reset">
<input type="submit" name="btn_delete" value="Delete" id="deleteBtn" class="btn btn-danger" disabled>
</div>
</form>
</div>
</div>

<script>

httpRequest = false;
if(window.XMLHttpRequest) {
    // Firefox, Opera
    httpRequest = new XMLHttpRequest();
    httpRequest.overrideMimeType('text/xml');
} else if(window.ActiveXObject) {
    // IE
    try {
        httpRequest = new ActiveXObject('Msxml2.XMLHTTP');
    } catch (e) {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
}

function getJSON(request,id) {
    httpRequest.abort();
    httpRequest.open('GET', request, true);
    httpRequest.onreadystatechange = function() {
        if(httpRequest.readyState == 4) {
            if(httpRequest.status == 200) {
            	var json = JSON.parse(httpRequest.responseText);
            	document.getElementById('message_en_'+id).textContent=json[0][0][0];
            }
        }
    }
    httpRequest.send(null);
}

window.onload = function(){
	for(var i=0; i<document.form.delete.length;i++){
		var messages_ja = document.getElementById('message_ja_'+i).textContent;
		var message = messages_ja.replace(" ","+");
		getJSON('http://translate.googleapis.com/translate_a/single?client=gtx&sl=ja&tl=en&dt=t&q=' + message,i);
	}
 }

function enableDelete() {
    var flag = false;
    for(var i=0; i<=document.form.delete.length-1;i++){
        if(document.form.delete[i].checked){
            flag = true;    
            document.getElementById('deleteBtn').disabled = false;
            document.getElementById('name').required = false;
            document.getElementById('submit').disabled = true;
        }
    }
    if(!flag){
        document.getElementById('deleteBtn').disabled = true;
        document.getElementById('submit').disabled = false;
    }
}


</script>

